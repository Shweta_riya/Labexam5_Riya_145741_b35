<?php
$start_date = new DateTime("1981-11-03");
$end_date = new DateTime("2013-09-04");

$difference = $end_date->diff($start_date);
echo "Difference between two dates : " . $difference->y . " years, " . $difference->m." months, ".$difference->d." days "."\n";

?>