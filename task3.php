<?php
class MyCalculator {
    public $val1, $val2;
    public function __construct( $val2, $val3 ) {
        $this->val1 = $val2;
        $this->val2 = $val3;
    }
    public function add() {
        return $this->val1 + $this->val2;
    }
    public function subtract() {
        return $this->val1 - $this->val2;
    }
    public function multiply() {
        return $this->val1 * $this->val2;
    }
    public function divide() {
        return $this->val1 / $this->val2;
    }
}
$mycalculation = new MyCalculator(12, 6);
echo "add =".$mycalculation-> add();
echo "<br>";
echo "mul=".$mycalculation-> multiply();
echo "<br>";
echo "sub =".$mycalculation-> subtract();
echo "<br>";
echo "div =".$mycalculation-> divide();
?>